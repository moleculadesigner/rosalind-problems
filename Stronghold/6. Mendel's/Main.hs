module Main where

import Data.Function (on)
import Data.Ratio

dominantProbability :: Integer -> Integer -> Integer -> Rational
dominantProbability a b c = 
    let p = (*) (a + b + c) (a + b + c - 1)
    in
        a * (a - 1) % p
        + 2 * a * b % p
        + 2 * a * c % p 
        + (b * (b - 1) % p) * (3 % 4)
        + (2 * b * c % p) *(1 % 2)

main = do
    putStr "AA: "
    a <- getLine >>= pure . read :: IO Integer
    putStr "Aa: "
    b <- getLine >>= pure . read :: IO Integer
    putStr "aa: "
    c <- getLine >>= pure . read :: IO Integer
    print $ fromRational $ dominantProbability a b c
