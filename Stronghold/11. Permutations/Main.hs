module Main where

import Data.List (permutations)
import qualified Data.Text as T

main = do
    putStr "Print the number: "
    n <- getLine >>= pure . read :: IO Int
    let perms = permutations [1 .. n]
    print . length $ perms
    putStr $ T.unpack . T.intercalate (T.singleton '\n')
        $ map (T.intercalate (T.singleton ' ') . map (T.pack . show)) perms
    putStr "\n"