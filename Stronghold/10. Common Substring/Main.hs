{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.ByteString.Lazy.Char8 as L
import qualified Data.Text                  as T
import qualified Data.Set                   as S
import           Utils.Fasta
import           System.Environment
import           Data.Maybe                      (fromJust, isJust)
import           Data.List                       (sortBy)
import           Data.Function                   (on)



main = do
    file <- getArgs >>= pure . head
    content <- readFile file
    putStrLn $ L.unpack . findCommon . sortByLen . entries $ content


entries :: String -> [L.ByteString]
entries =
    map (fastaSequence . fromJust)
    . filter isJust
    . parseFasta
    . L.pack
    . T.unpack
    . T.strip
    . T.pack

sortByLen :: [L.ByteString] -> [L.ByteString]
sortByLen =
    sortBy (on compare L.length)

allSubstrings :: L.ByteString -> [L.ByteString]
allSubstrings = reverse
    . sortByLen
    . S.toList
    . S.fromList
    . concatMap L.inits
    . L.tails

findCommon :: [L.ByteString] -> L.ByteString
findCommon []       = ""
findCommon [sq]     = sq
findCommon (sq:sqs) = probe (allSubstrings sq) $ map L.tails sqs 
  where
    probe :: [L.ByteString] -> [[L.ByteString]] -> L.ByteString
    probe [] _ = ""
    probe (substr:substrs) target =
        if all (any (L.isPrefixOf substr)) target
        then substr
        else probe substrs target