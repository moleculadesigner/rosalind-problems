{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.ByteString.Lazy.Char8 as L
import           Data.List                       (sort, sortBy)
import           Data.Function                   (on)
import           Control.Monad                   ((<=<))
import           System.Environment
import           System.Exit


-- | Creates sorted list of all suffixes with theri starting position
suffixList :: L.ByteString -> [(L.ByteString, Integer)]
suffixList =
    sortBy (on compare fst)
    . flip zip [1 ..]
    . init 
    . L.tails

findSubstring :: L.ByteString -> L.ByteString -> [Integer]
findSubstring needle =
    sort
    . map snd
    . filter (L.isPrefixOf needle . fst)
    . suffixList

main = do
    args <- getArgs
    (haystack:needle:_) <- return . L.lines . L.pack <=< readFile $ head args
    putStrLn $ L.unpack . L.intercalate " " $ map (L.pack . show) $ findSubstring needle haystack

