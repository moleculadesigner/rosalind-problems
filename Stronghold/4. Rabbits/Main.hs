module Main where

import Control.Monad ((<=<), (>=>))

grow :: Integer -> (Integer, Integer) -> (Integer, Integer)
grow litter (adult, young) = (adult + young, adult * litter)

burst :: Integer -> (Integer, Integer) -> Integer -> Integer
burst litter population time
    | time < 2  = uncurry (+) population
    | otherwise = burst litter (grow litter population) (time - 1)

main = do
    putStrLn "Enter n:"
    time <- (getLine >>= return . read) :: IO Integer
    putStrLn "Enter k:"
    litter <- (getLine >>= return . read) :: IO Integer
    print $ burst litter (0, 1) time
