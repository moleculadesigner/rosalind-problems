module Main where

import System.Environment
import System.Exit
import Control.Monad ((<=<))
import Data.Maybe (fromMaybe)

hamming :: String -> String -> Maybe Integer
hamming pattern derivative
    | length pattern /= length derivative = Nothing
    | otherwise = Just $
        sum $ zipWith (\a b -> if a == b then 0 else 1) pattern derivative

main = do
    args <- getArgs
    seqs <- return . lines <=< readFile $ head args
    print . fromMaybe (-1) $ hamList seqs
  where
    hamList (x:y:_) = hamming x y
    hamList _       = Nothing

