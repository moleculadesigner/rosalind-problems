module Utils.Fasta
    ( wrap
    , FastaEntry(..)
    , parseFasta
    --, groupLists
    ) where

import qualified Data.Text as T
import qualified Data.ByteString.Lazy.Char8 as L


data FastaEntry = FastaEntry
    { fastaHeader   :: L.ByteString
    , fastaSequence :: L.ByteString
    } deriving (Eq, Show)


wrap :: (L.ByteString -> L.ByteString) -> String -> String
wrap f = L.unpack . f . L.pack

groupLists :: (a -> Bool) -> [a] -> [[a]]
groupLists predicate list = reverse . map reverse $ groupLists' predicate list [[]]
  where
    groupLists' _ [] groups = groups
    groupLists' pred (x:xs) [[]] = groupLists' pred xs [[x]]
    groupLists' pred (x:xs) (group:groups) = case pred x of
        True  -> groupLists' pred xs ([x]:group:groups)
        False -> groupLists' pred xs ((x:group):groups)


parseFasta :: L.ByteString -> [Maybe FastaEntry]
parseFasta = map parseEntry . groupLists validHeader . L.lines
  where
    parseEntry :: [L.ByteString] -> Maybe FastaEntry
    parseEntry (header:sequence) = 
        if 
            validHeader header &&
            L.concat sequence /= L.pack ""
        then Just $ FastaEntry
            (L.drop 1 header)
            (L.concat sequence)
        else Nothing
    validHeader = L.isPrefixOf $ L.singleton '>'