{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GADTs             #-}

module Utils.Nucleotides
    ( Nucleobase(..) -- Class
    , RnaBase(..)
    , DnaBase(..)
    , toDna
    , readDna
    , toRna
    , readRna
    , isPurine
    , isPyrimidine
    , isGC
    , replicateNa
    , transcribeNa
    , revertNa
    ) where


import Data.Char (toUpper)


{- | Types for nucleobases representation.

All unknown nucleobases are marked as X losing their context. Handling of uncanonical
bases may be added soon.
-}

class Nucleobase a where
    {-# MINIMAL isBase | complement #-}

    isBase :: a -> Bool
    isBase = const False

    complement :: a -> a
    complement = id


data DnaBase = Ad | Cd | Gd | Td | Xd
    deriving (Eq)

instance Show DnaBase where
    show Ad = "A"
    show Cd = "C"
    show Gd = "G"
    show Td = "T"
    show Xd = "X"

    showsPrec _ = showString . show

    showList l = showString $ concatMap show l

instance Nucleobase DnaBase where
    isBase Xd = False
    isBase _  = True

    complement Ad = Td
    complement Cd = Gd
    complement Gd = Cd
    complement Td = Ad
    complement Xd = Xd


data RnaBase = Ar | Cr | Gr | Ur | Xr
    deriving (Eq)

instance Show RnaBase where
    show Ar = "A"
    show Cr = "C"
    show Gr = "G"
    show Ur = "U"
    show Xr = "X"

    showsPrec _ = showString . show

    showList l = showString $ concatMap show l

instance Nucleobase RnaBase where
    isBase Xr = False
    isBase _  = True

    complement Ar = Ur
    complement Cr = Gr
    complement Gr = Cr
    complement Ur = Ar
    complement Xr = Xr

toDna :: Char -> DnaBase
toDna c = case toUpper c of
    'A' -> Ad
    'C' -> Cd
    'G' -> Gd
    'T' -> Td
    _   -> Xd

readDna :: String -> [DnaBase]
readDna = map toDna

toRna :: Char -> RnaBase
toRna c = case toUpper c of
    'A' -> Ar
    'C' -> Cr
    'G' -> Gr
    'U' -> Ur
    _   -> Xr

readRna :: String -> [RnaBase]
readRna = map toRna
    

isPurine :: (Nucleobase a, Show a) => a -> Bool
isPurine base = case show base of
    "C" -> True
    "T" -> True
    "U" -> True
    _   -> False

isPyrimidine :: (Nucleobase a, Show a) => a -> Bool
isPyrimidine base = not (isPurine base) && isBase base

isGC :: (Nucleobase a, Show a) => a -> Bool
isGC base = case show base of
    "C" -> True
    "G" -> True
    _   -> False

replicateNa :: Nucleobase a => [a] -> [a]
replicateNa = reverse . map complement

complDnaRna :: DnaBase -> RnaBase
complDnaRna base = case base of
    Ad -> Ur
    Cd -> Gr
    Gd -> Cr
    Td -> Ar
    Xd -> Xr

transcribeNa :: [DnaBase] -> [RnaBase]
transcribeNa = reverse . map complDnaRna

complRnaDna :: RnaBase -> DnaBase
complRnaDna base = case base of
    Ar -> Td
    Cr -> Gd
    Gr -> Cd
    Ur -> Ad
    Xr -> Xd

revertNa :: [RnaBase] -> [DnaBase]
revertNa = reverse . map complRnaDna