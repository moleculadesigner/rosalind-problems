module Utils.Translation
    ( AminoAcid(..)
    , TranscriptionError(..)
    , toAminoAcid
    , readProt
    , translateNa
    ) where

import qualified Utils.Nucleotides            as N
import           Control.Applicative (liftA2)
import           Data.Char (toUpper)


data TranscriptionError
    = UnknownBaseError
    | IncompleteCodonError
    deriving (Show, Eq)


data AminoAcid
    = A -- Alanine
    | R -- Arginine
    | N -- Asparagine
    | D -- Aspartate
    | C -- Cysteine
    | E -- Glutamate
    | Q -- Glutamine
    | G -- Glycine
    | H -- Histidine
    | I -- Isoleucine
    | L -- Leucine
    | K -- Lysine
    | M -- Methionine
    | F -- Phenylalanine
    | P -- Proline
    | S -- Serine
    | T -- Threonine
    | W -- Tryptophane
    | Y -- Tyrosine
    | V -- Valine
    | Stop -- Not an AA, but necessary value for translation 
    | X -- Unknown / Error
    deriving (Eq)

instance Show AminoAcid where
    show A    = "A"
    show R    = "R"
    show N    = "N"
    show D    = "D"
    show C    = "C"
    show E    = "E"
    show Q    = "Q"
    show G    = "G"
    show H    = "H"
    show I    = "I"
    show L    = "L"
    show K    = "K"
    show M    = "M"
    show F    = "F"
    show P    = "P"
    show S    = "S"
    show T    = "T"
    show W    = "W"
    show Y    = "Y"
    show V    = "V"
    show Stop = "*"
    show X    = "X"

    showsPrec _ = showString . show

    showList = showString . concatMap show


data Codon = Codon !N.RnaBase !N.RnaBase !N.RnaBase deriving (Eq)

instance Show Codon where
    show (Codon a b c) = showList [a, b, c] ""

    showsPrec _ (Codon a b c) = showList [a, b, c]


toAminoAcid :: Char -> AminoAcid
toAminoAcid char = case toUpper char of
    'A' -> A
    'R' -> R
    'N' -> N
    'D' -> D
    'C' -> C
    'E' -> E
    'Q' -> Q
    'G' -> G
    'H' -> H
    'I' -> I
    'L' -> L
    'K' -> K
    'M' -> M
    'F' -> F
    'P' -> P
    'S' -> S
    'T' -> T
    'W' -> W
    'Y' -> Y
    'V' -> V
    '*' -> Stop
    _   -> X

readProt :: String -> [AminoAcid]
readProt = map toAminoAcid

seqToCodons :: [N.RnaBase] -> Either TranscriptionError [Codon]
seqToCodons [] = Right []
seqToCodons (base1:base2:base3:sequence) = 
    let c = validCodon $ Codon base1 base2 base3
    in  liftA2 (:) c (seqToCodons sequence)
seqToCodons _ = Left IncompleteCodonError

validCodon :: Codon -> Either TranscriptionError Codon
validCodon (Codon base1 base2 base3)
    | elem N.Xr [base1, base2, base3] = Left UnknownBaseError
    | otherwise                      = Right $ Codon base1 base2 base3

translateNa :: [N.RnaBase] -> Either TranscriptionError [AminoAcid]
translateNa sequnce = seqToCodons sequnce >>= pure . map codonToAa

codonToAa :: Codon -> AminoAcid
codonToAa (Codon a b c) =
    case a of 
        N.Ur -> case b of
            N.Ur -> case c of
                N.Ur -> F
                N.Cr -> F
                N.Ar -> L
                N.Gr -> L
                _  -> X
            N.Cr -> case c of
                N.Ur -> S
                N.Cr -> S
                N.Ar -> S
                N.Gr -> S
                _  -> X
            N.Ar -> case c of
                N.Ur -> Y   
                N.Cr -> Y   
                N.Ar -> Stop
                N.Gr -> Stop
                _  -> X
            N.Gr -> case c of
                N.Ur -> C   
                N.Cr -> C   
                N.Ar -> Stop
                N.Gr -> W
                _  -> X
            _  -> X
        N.Cr -> case b of
            N.Ur -> case c of
                N.Ur -> L
                N.Cr -> L
                N.Ar -> L
                N.Gr -> L
                _  -> X
            N.Cr -> case c of
                N.Ur -> P
                N.Cr -> P
                N.Ar -> P
                N.Gr -> P
                _  -> X
            N.Ar -> case c of
                N.Ur -> H
                N.Cr -> H
                N.Ar -> Q
                N.Gr -> Q
                _  -> X
            N.Gr -> case c of
                N.Ur -> R
                N.Cr -> R
                N.Ar -> R
                N.Gr -> R
                _  -> X
            _  -> X
        N.Ar -> case b of
            N.Ur -> case c of
                N.Ur -> I
                N.Cr -> I
                N.Ar -> I
                N.Gr -> M
                _  -> X
            N.Cr -> case c of
                N.Ur -> T
                N.Cr -> T
                N.Ar -> T
                N.Gr -> T
                _  -> X
            N.Ar -> case c of
                N.Ur -> N
                N.Cr -> N
                N.Ar -> K
                N.Gr -> K
                _  -> X
            N.Gr -> case c of
                N.Ur -> S
                N.Cr -> S
                N.Ar -> R
                N.Gr -> R
                _  -> X
            _  -> X
        N.Gr -> case b of
            N.Ur -> case c of
                N.Ur -> V
                N.Cr -> V
                N.Ar -> V
                N.Gr -> V
                _  -> X
            N.Cr -> case c of
                N.Ur -> A
                N.Cr -> A
                N.Ar -> A
                N.Gr -> A
                _  -> X
            N.Ar -> case c of
                N.Ur -> D
                N.Cr -> D
                N.Ar -> E
                N.Gr -> E
                _  -> X
            N.Gr -> case c of
                N.Ur -> G
                N.Cr -> G
                N.Ar -> G
                N.Gr -> G
                _  -> X
            _  -> X
        _  -> X