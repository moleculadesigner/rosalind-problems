module Main where

    import System.Environment
    import System.Exit
    import Data.Traversable (traverse)
    import Control.Monad ((<=<))
    
    import Revc
    
    main :: IO ()
    main = do
        args <- getArgs
        seqs <- traverse readFile args
        trSeqs <- traverse (return . transcribe) seqs
        traverse putStrLn trSeqs
        exitWith ExitSuccess