module Revc where

import qualified Data.Text as T
import           Data.Char      (toUpper)

complement :: Char -> Char
complement base = case toUpper base of
    'A' -> 'T'
    'T' -> 'A'
    'C' -> 'G'
    'G' -> 'C'
    _   -> base

transcribe :: String -> String
transcribe =
    T.unpack .
    T.reverse .
    T.map complement .
    T.pack