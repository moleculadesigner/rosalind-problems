module Main where

import qualified Data.Text as T
import System.Environment
import System.Exit
import Data.Traversable (traverse)
import Control.Monad ((<=<))

transcribe :: T.Text -> T.Text
transcribe =
    let tToU base = if base == 'T'
        then 'U'
        else base
    in
    T.map tToU


main :: IO ()
main = do
    args <- getArgs
    seqs <- traverse (return . T.pack <=< readFile) args
    trSeqs <- traverse (return . transcribe) seqs
    traverse (putStrLn . T.unpack) trSeqs
    exitWith ExitSuccess