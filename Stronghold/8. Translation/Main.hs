module Main where

import qualified Utils.Nucleotides as N
import qualified Utils.Translation as Prot

import System.Environment
import System.Exit

main = do
    args <- getArgs
    sequence <- readFile $ head args
    result <- pure $ (Prot.translateNa . N.readRna) sequence
    print . (\(Right x) -> x) $ result -- LET IT CRASH!
