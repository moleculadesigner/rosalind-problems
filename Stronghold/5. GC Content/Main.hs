module Main where

import Utils.Fasta
import System.Environment
import System.Exit
import Data.Char (toUpper)
import Data.Traversable (traverse)
import Control.Monad ((<=<))
import qualified Data.Text as T
import Data.List (foldl')
import Data.Maybe (catMaybes)
import Text.Printf


gcContent :: FastaEntry -> Double
gcContent fasta = 
    let gOrC base = if toUpper base == 'G' || toUpper base == 'C'
            then 1.0
            else 0.0
        len = fromIntegral . T.length $ (fastaSequence fasta)
    in 100.0 * (foldl' (\acc bs -> acc + gOrC bs) 0.0 $ T.unpack . fastaSequence $ fasta) / len

printGc :: FastaEntry -> String
printGc fasta = T.unpack (fastaHeader fasta) ++ "\n" ++ printf "%.6f" (gcContent fasta)

main :: IO ()
main = do
    args <- getArgs
    fastas <- pure . parseFasta . T.pack <=< readFile $ head args
    traverse (putStrLn . printGc) (catMaybes fastas)
    exitWith ExitSuccess