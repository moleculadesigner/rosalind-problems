module DNA (baseCount) where

import Data.Char (toUpper)


baseCount :: String -> (Integer, Integer, Integer, Integer)
baseCount = baseCountHelper (0, 0, 0, 0)
  where
    baseCountHelper (a, c, g, t) "" = (a, c, g, t)
    baseCountHelper (a, c, g, t) (base:sequence) =
        case toUpper base of
            'A' -> baseCountHelper (a + 1, c, g, t) sequence
            'C' -> baseCountHelper (a, c + 1, g, t) sequence
            'G' -> baseCountHelper (a, c, g + 1, t) sequence
            'T' -> baseCountHelper (a, c, g, t + 1) sequence
            _   -> baseCountHelper (a, c, g, t)     sequence