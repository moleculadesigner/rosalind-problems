module Main where

import DNA

import System.Environment
import System.Exit
import Data.Traversable (traverse)
import Control.Monad ((<=<))


main :: IO ()
main = do
    args <- getArgs
    counts <- traverse ((return . baseCount) <=< readFile) args
    let
        prettify (a, c, g ,t) = show a ++ " " ++ show c ++ " " ++ show g ++ " " ++ show t in 
        traverse (putStrLn . prettify) counts
    exitWith ExitSuccess